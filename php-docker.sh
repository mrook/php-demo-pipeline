#!/bin/bash

apt-get update -yqq
apt-get install git unzip libzip-dev -yqq
docker-php-ext-install zip
curl -sS https://getcomposer.org/installer | php
php composer.phar global require hirak/prestissimo
php composer.phar install